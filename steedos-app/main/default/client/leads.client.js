Steedos.CRM = {};

Steedos.CRM.showLeadConvertForm = function (fields, formId, doc, onConfirm, title) {
    var schema = Creator.getObjectSchema({ fields: fields });
    Modal.show("quickFormModal", { formId: formId, title: title || "Convert Lead", confirmBtnText: `Convert`, schema: schema, autoExpandGroup: true, doc: doc, onConfirm: onConfirm }, {
        backdrop: 'static',
        keyboard: true
    });
}

Steedos.CRM.convertLead = function (record) {
    if (record.converted) {
        toastr.error(t("This lead has already been converted and cannot be converted again!"));
        return;
    }
    const record_id = record._id;
    const object_name = "leads";
    let doc = {};
    doc.new_account_name = record.company;
    doc.new_contact_name = record.name;
    doc.new_opportunity_name = `${doc.new_account_name}-`;
    doc.omit_new_opportunity = false;
    doc.record_owner_id = Steedos.userId();
    var formId = 'leadConvertForm';
    Steedos.CRM.showLeadConvertForm({
        new_account_name: {
            label: "New Account Name",
            type: 'text',
            is_wide: true,
            group: "Account"
        },
        is_lookup_account: {
            label: "Select Existing Account",
            type: 'toggle',
            group: "Account"
        },
        lookup_account: {
            label: "Existing Account",
            type: 'lookup',
            reference_to: 'accounts',
            group: "Account"
        },
        new_contact_name: {
            label: "New Contact Name",
            type: 'text',
            is_wide: true,
            group: "Contact"
        },
        is_lookup_contact: {
            label: "Select Existing Contact",
            type: 'toggle',
            group: "Contact"
        },
        lookup_contact: {
            label: "Existing Contact",
            type: 'lookup',
            reference_to: 'contacts',
            depend_on: ["is_lookup_contact", "is_lookup_account", "lookup_account"],
            optionsFunction: function (values) {
                let { is_lookup_contact, is_lookup_account, lookup_account } = values;
                if (!is_lookup_contact) {
                    return [];
                }
                let options = {
                    $select: 'name'
                };
                let queryFilters = [["account", "=", null]];
                if (is_lookup_account && lookup_account) {
                    queryFilters.push("or");
                    queryFilters.push(["account", "=", lookup_account]);
                }
                let steedosFilters = require("@steedos/filters");
                let odataFilter = steedosFilters.formatFiltersToODataQuery(queryFilters);
                options.$filter = odataFilter;
                let records = Creator.odata.query('contacts', options, true);
                let result = [];
                records.forEach(function (item) {
                    result.push({
                        label: item.name,
                        value: item._id
                    });
                });
                return result;
            },
            group: "Contact"
        },
        force_update_contact_lead_source: {
            label: "Update Lead Source",
            type: 'toggle',
            group: "Contact"
        },
        new_opportunity_name: {
            label: "New Opportunity Name",
            type: 'text',
            is_wide: true,
            group: "Opportunity"
        },
        is_lookup_opportunity: {
            label: "Select Existing Opportunity",
            type: 'toggle',
            group: "Opportunity"
        },
        lookup_opportunity: {
            label: "Existing Opportunity",
            type: 'lookup',
            reference_to: 'opportunity',
            depend_on: ["is_lookup_opportunity", "is_lookup_account", "lookup_account"],
            optionsFunction: function (values) {
                let { is_lookup_opportunity, is_lookup_account, lookup_account } = values;
                if (!is_lookup_opportunity) {
                    return [];
                }
                let options = {
                    $select: 'name'
                };
                let queryFilters = [["account", "=", null]];
                if (is_lookup_account && lookup_account) {
                    queryFilters.push("or");
                    queryFilters.push(["account", "=", lookup_account]);
                }
                let steedosFilters = require("@steedos/filters");
                let odataFilter = steedosFilters.formatFiltersToODataQuery(queryFilters);
                options.$filter = odataFilter;
                let records = Creator.odata.query('opportunity', options, true);
                let result = [];
                records.forEach(function (item) {
                    result.push({
                        label: item.name,
                        value: item._id
                    });
                });
                return result;
            },
            group: "Opportunity"
        },
        omit_new_opportunity: {
            label: "Do not create an opportunity upon conversion",
            type: 'toggle',
            group: "Opportunity"
        },
        record_owner_id: {
            label: "Record Owner",
            type: 'lookup',
            reference_to: 'users',
            required: true,
            group: "Other"
        }
    }, formId, doc, function (formValues, e, t) {
        let insertDoc = formValues.insertDoc;
        var result = Steedos.authRequest(`/api/v4/${object_name}/${record_id}/convert`, { type: 'post', async: false, data: JSON.stringify(insertDoc) });
        if (result && result.state === 'SUCCESS') {
            FlowRouter.reload();
            Modal.hide(t);
            Steedos.CRM.alertLeadConvertedRecords(record);
        }
    })
}

Steedos.CRM.alertLeadConvertedRecords = function (record) {
    const record_id = record._id;
    const object_name = "leads";
    const fields = "converted_account,converted_contact,converted_opportunity";
    // Note: Here we pass two fields parameters, the second one is expand, do not delete
    const converteds = Creator.odata.get(object_name, record_id, fields, fields);
    let doc = {};
    if (converteds.converted_account) {
        doc.account_name = converteds.converted_account._NAME_FIELD_VALUE;
        doc.account_url = Creator.getObjectAbsoluteUrl("accounts", converteds.converted_account._id);
    }
    if (converteds.converted_contact) {
        doc.contact_name = converteds.converted_contact._NAME_FIELD_VALUE;
        doc.contact_url = Creator.getObjectAbsoluteUrl("contacts", converteds.converted_contact._id);
    }
    if (converteds.converted_opportunity) {
        doc.opportunity_name = converteds.converted_opportunity._NAME_FIELD_VALUE;
        doc.opportunity_url = Creator.getObjectAbsoluteUrl("opportunity", converteds.converted_opportunity._id);
    }
    let html = `
        <div class="grid grid-cols-1 lg:gap-2">
            <div class="flex items-start">
                <div class="ml-4">
                    <p class="text-gray-900"><span>Account: </span><a href="${doc.account_url}" target="_blank">${doc.account_name ? doc.account_name : ""}</a></p>
                </div>
            </div>
            <div class="flex items-start">
                <div class="ml-4">
                    <p class="text-gray-900"><span>Contact: </span><a href="${doc.contact_url}" target="_blank">${doc.contact_name ? doc.contact_name : ""}</a></p>
                </div>
            </div>
            <div class="flex items-start">
                <div class="ml-4">
                    <p class="text-gray-900"><span>Opportunity: </span><a href="${doc.opportunity_url}" target="_blank">${doc.opportunity_name ? doc.opportunity_name : ""}</a></p>
                </div>
            </div>
        </div>
    `;
    swal({
        title: "Lead Converted",
        text: html,
        html: true,
        type: "success",
        confirmButtonText: t('OK')
    },
        () => {
            sweetAlert.close();
        }
    );
}