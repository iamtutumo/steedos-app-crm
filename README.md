<!--
 * @Author: Tutu Moses etu.moses@gmail.com
 * @Date: 2024-12-03 14:34:20
 * @LastEditors: Tutu Moses etu.moses@gmail.com
 * @LastEditTime: 2024-12-03 14:37:07
 * @FilePath: /steedos-app-crm/README.md
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
-->
# Salesforce Alternative

## Run your salesforce

### Install dependences

- [NodeJS v10.0 or above](https://nodejs.org/en/)
- [MongoDB Community Server v3.4 or above](https://www.mongodb.com/download-center/community)

### Install node modules

```bash
npm i yarn -g
yarn
```

### Run Server

```bash
yarn start
```

navigate to http://127.0.0.1:5000

## Customize your salesforce

- [Configuration](./steedos-config.yml)
- [Apps](./src/apps)
- [Objects](./src/objects)